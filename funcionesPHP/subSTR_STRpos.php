<?php
/*
 * Extrar todo el texto que se encuentra antes de 
 * un caracter especifico de una cadena.
 */ 
$texto_completo = "hola?comoandastodobien";//Cadena de texto original
$caracter = "?";//Caracter a encontrar
$texto_extraido = substr($texto_completo, 0, strpos($texto_completo, $caracter));
echo $texto_extraido; //imprime "hola"

?>
